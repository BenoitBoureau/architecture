import socket
import select
import time

hote = ''
port = 12800

connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_principale.bind((hote, port))

# le serveur accepte au maximum 5 connexion
connexion_principale.listen(5)

print("Le serveur écoute à présent sur le port {}".format(port))

serveur_lance = True
clients_connectes = []
lastConnectedPlayers= 0

try:

	# tant que le serveur est lancé on poursuit
	while serveur_lance:

		# vérifier que de nouveaux clients ne demandent pas à se connecter
		# On attend maximum 50ms
		connexions_demandees, wlist, xlist = select.select([connexion_principale],[], [], 2)


		# Quand on a une connexion qui arrive on entre dans le for (boucle pour la récupération donc toutes les 50ms)
		for connexion in connexions_demandees:

			connexion_avec_client, infos_connexion = connexion.accept()
		
			# On ajoute le socket connecté à la liste des clients
			clients_connectes.append(connexion_avec_client)



		# CAS DECONNEXION

		if(lastConnectedPlayers > len(clients_connectes)):

			lastConnectedPlayers = len(clients_connectes)

			for client in clients_connectes:
				client.send(b"Un joueur vient de se deconnecter")
				log = str(len(clients_connectes)) + " joueurs restants"
				client.send(str.encode(log))

			print("Un joueur à quitté la partie !")

			if(len(clients_connectes) < 2):
				print("Nombre de joueur insuffisant : Fin de la partie")

				for client in clients_connectes:
					client.send(b"Nombre de joueur insuffisant : Fin de la partie")


		# CAS CONNEXION

		elif(lastConnectedPlayers < len(clients_connectes)):

			print(str(len(clients_connectes)) + " clients connectés")


			if(lastConnectedPlayers != 0):
				for client in clients_connectes:
					client.send(b"Un joueur vient de se connecter")
			

			lastConnectedPlayers = len(clients_connectes)


			if(len(clients_connectes) < 2):

				print("pas assez de joueurs")
				clients_connectes[0].send(b"Nombre de joueur insufisant ! " )

			else:
				print("début de la partie")


				for client in clients_connectes:

					client.send(b"Question 1 :")

					for secondesRestantes in range(0,10):
						client.send(str(secondesRestantes).encode())
					time.sleep(1)

				
				
				clients_a_lire = []
				try:
					clients_a_lire, wlist, xlist = select.select(clients_connectes,[], [], 0.05)
				except select.error:
					pass
				else:

					# On parcourt la liste des clients à lire
					for client in clients_a_lire:
						# Client est de type socket
						msg_recu = client.recv(1024)
						# Peut planter si le message contient des caractères spéciaux
						msg_recu = msg_recu.decode()
						print("Reçu {}".format(msg_recu))
						client.send(b"OK")

						if msg_recu == "fin":
							serveur_lance = False


except KeyboardInterrupt:
	for client in clients_connectes:
		client.close()
	raise


except:
	for client in clients_connectes:
		client.close()
	raise
	
connexion_principale.close()
