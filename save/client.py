import socket

hote = "localhost"
port = 12800

connexion_avec_serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connection pour localhost sur le port 12800
connexion_avec_serveur.connect((hote, port))

print("Connexion établie avec le serveur sur le port {}".format(port))

# b = type bytes
msg_a_envoyer = b""





msg_recu = connexion_avec_serveur.recv(1024)
print(msg_recu.decode())




# Tant que le message envoyé n'est pas "fin" on boucle
while msg_a_envoyer != b"fin":

	# on met dans la variable msg_a_envoyer ce qui est après "> "
	msg_a_envoyer = input("> ")

	# on encode les bytes en string
	msg_a_envoyer = msg_a_envoyer.encode()

	# On envoie le message stocké dans la variable dans la connexion faite plut tôt
	connexion_avec_serveur.send(msg_a_envoyer)

	# valide la récéption du message
	# msg_recu = connexion_avec_serveur.recv(1024)
	# print(msg_recu.decode())

	# end while

# Si le message est "fin" on entre dans ce cas - log + on ferme la connexion
print("Fermeture de la connexion")
connexion_avec_serveur.close()
