2- Retour de l’état de la connexion d’un client.
L’objectif de cet exemple, c’est d’ajouter des tests sur l’état de la
transmission des données d’un client ( identifié par son adresse IP) sur le
serveur ... et afficher si la connexion a échoué ou réussi ... ça nous
permettra d’afficher un message d’erreur dans le cas d’une interruption de
transmission.
