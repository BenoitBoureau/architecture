4- Envoie d’une réponse suite à des messages envoyer par le client.
- À chaque message reçu, le serveur envoie en retour le message 'OK'.
- Le programme client va tenter de se connecter sur le port 12800 de la machine locale. Il
demande à l'utilisateur de saisir quelque chose au clavier et envoie ce quelque chose au
serveur, puis attend sa réponse.
