3- Afficher sur le Serveur les données saisies sur le client :
Dans cette sous partie, nous allons apprendre à faire :
- Un client qui envoie des messages au serveur
- Un serveur qui reçoit et affiche les messages reçus.
