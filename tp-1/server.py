import socket
import select
hote = ''
port = 12800

connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_principale.bind((hote, port))
connexion_principale.listen(5)

print("Le serveur écoute à présent sur le port {}".format(port))
serveur_lance = True
connectedPlayers = []

lastConnectedPlayers= 0



###############################################
################## FUNCTIONS ##################
###############################################


# function sendAll
# @description : Envoyer un message à tous les joueurs
# @param msg : Le message à envoyer
#
def sendAll(msg):
	for player in connectedPlayers:
		player.send(msg.encode())


# function closeAll
# @description : Deconnecte tous les joueurs
#
def closeall():
	for player in connectedPlayers:
		player.close()


###############################################
################# TRAITEMENT ##################
###############################################


# Si le serveur ne rencontre pas de problèmes
try:

	while serveur_lance:

		# vérifier que de nouveaux joueurs ne demandent pas à se connecter
		connexions_demandees, wlist, xlist = select.select([connexion_principale],[], [], 1)

		for connexion in connexions_demandees:
		
			connexion_avec_joueurs, infos_connexion = connexion.accept()
			connectedPlayers.append(connexion_avec_joueurs)


		print(len(connectedPlayers))

		# Si un player se connecte
		if(lastConnectedPlayers < len(connectedPlayers)):
			sendAll("Un joueur vient de se connecter")
		elif(lastConnectedPlayers > len(connectedPlayers)):
			sendAll("Un joueur vient de se deconnecter")


		# Les joueurs renvoyés par select sont ceux devant être lus (recv)
		playersToRead = []
		try:
			playersToRead, wlist, xlist = select.select(connectedPlayers,[], [], 0.05)
		except select.error:
			pass
		else:
			# On parcourt la liste des joueurs à lire
			for player in playersToRead:
				# player est de type socket
				msg_recu = player.recv(1024)
				# Peut planter si le message contient des caractères spéciaux
				msg_recu = msg_recu.decode()
				print("Reçu {}".format(msg_recu))
				
				# if msg_recu == "fin":
				# 	serveur_lance = False

		lastConnectedPlayers = len(connectedPlayers)


	print("Fermeture des connexions")
	connexion_principale.close()

except:
	connexion_principale.close()
	closeall()
	raise