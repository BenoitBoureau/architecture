import socket, sys, threading

#************************************************#
#**************** INITIALISATION ****************#
#************************************************#


HOTE = "localhost"
PORT = 50026


#************************************************#
#******************** CLASS *********************#
#************************************************#


# Class Reception
# @param - On envoie la variable "mysocket" avec la connexion au serveur
# @fonctionnement - On attend la reception d'un message serveur

class Reception(threading.Thread):

    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn


    def run(self):
    # On "patiente" - Tant qu'on a pas reçu de message on continue d'attendre, dès qu'on reçoit un message on l'affiche
        while True:
            try:
                # on récupère le message, on l'encode en UTF 8 et on l'affiche pour le client
                message_recu = self.connexion.recv(4096)
                message_recu = message_recu.decode(encoding='UTF-8')
                print(message_recu)
            except:
                break
            
        print("La réception est arrêtée. Connexion interrompue.")
        self.connexion.close()


# Class Emission
# @param - On envoie la variable "mysocket" avec la connexion au serveur
# @fonctionnement - On envoie le message du client au serveur

class Emission(threading.Thread):
    
    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn   # réf. du socket de connexion

    def run(self):
    # On "patiente" - Tant qu'on a pas de message en envoyer on continue d'attendre, dès qu'on a un input on l'envoie
        while True:
            message_emis = input()
            try:
                # On envoie au client le message "message_emis" écrit par le client
                self.connexion.send(bytes(message_emis,'UTF-8'))
            except:
                # fin du thread
                break
            
        print("L'émission est arrêtée. Connexion interrompue.")
        self.connexion.close()


#************************************************#
#**************** FONCTIONNEMENT ****************#
#************************************************#


mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)

try:
    mySocket.connect((HOTE, PORT))
except socket.error:
    print("La connexion a échoué.")
    sys.exit()


# Dialogue avec le serveur : on lance deux threads pour gérer indépendamment l'émission et la réception des messages

# Reception du serveur
th_R = Reception(mySocket)
th_R.start()

# Emission du client
th_E = Emission(mySocket)
th_E.start()
