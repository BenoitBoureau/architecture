Cours d'architecture matérielle et logicielle

- TD client serveur
	
```python3 server.py```
```python3 client.py```

- 1 - Envoyer un message d’un client au serveur en écoute
- 2 - Retour de l’état de la connexion d’un client
- 3 - Afficher sur le Serveur les données saisies sur le client
- 4 - Envoie d’une réponse suite à des messages envoyer par le client
- 5 - Se connecter à plusieurs sur le serveur ( amélioration de l’exemple précédent ..)
- 6 - Mettre en place un échange client/serveur avec le protocole UDP Fichier Server.py
